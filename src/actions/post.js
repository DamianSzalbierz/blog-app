import { getPosts, getPost, savePost, updatePost, removePost } from '../lib/postServices';
import { POSTS_LOAD, POST_ADD, POST_SHOW, POST_UPDATE, POST_DELETE } from '../lib/types';

const loadPosts = (posts) => ({type: POSTS_LOAD, payload: posts})
const addPost = (post) => ({type: POST_ADD, payload: post})
const showPost = (post) => ({type: POST_SHOW, payload: post})
const upgradePost = post => ({type: POST_UPDATE, payload: post})
const destroyPost = id => ({type: POST_DELETE, payload: id})

export const fetchPosts = () => {
  return ((dispatch) => {
    getPosts()
      .then(posts => dispatch(loadPosts(posts)))
  })
}

export const newPost = ({author, title, content}, callback) => {
  return (dispatch) => {
    savePost({author, title, content})
      .then(post => dispatch(addPost(post)))
      .then(callback())
  }
}

export const fetchPost = id => {
  return (dispatch) => {
    getPost(id)
      .then(post => dispatch(showPost(post)))
  }
}

export const changePost = (post, callback) => {
  return dispatch => {
    updatePost(post)
      .then(data => dispatch(upgradePost(data)))
      .then(callback())
  }
}

export const deletePost = (id) => {
  return (dispatch) => {
    removePost(id)
      .then(() => dispatch(destroyPost(id)))
  }
}
