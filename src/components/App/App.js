import React, { Component } from 'react';
import './App.css';
import {
  Route,
  Redirect,
  Switch
} from 'react-router-dom';
import PostsPage from '../pages/PostsPage';
import PostPage from '../pages/PostPage';
import PostNewPage from '../pages/PostNewPage';
import PostEditPage from '../pages/PostEditPage';
import Header from '../header/Header';

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className='ui container'>
          <Switch>
            <Route path='/posts/new' exact component={PostNewPage} />
            <Route path='/posts/:id/edit' component={PostEditPage} />
            <Route path='/posts/:id' component={PostPage} />
            <Route path='/posts' component={PostsPage} />
            <Redirect to='/posts' />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
