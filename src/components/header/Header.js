import React from 'react';
import { Menu } from 'semantic-ui-react';
import { withRouter } from 'react-router-dom';

class Header extends React.Component {
  state = { activeItem: 'home' }

  handleItemClick = (e, { name, routeto }) => {
    this.setState({ activeItem: name })
    this.props.history.push(routeto)
  }

  componentDidMount = () => {
    const { pathname } = this.props.history.location;
    if (pathname && pathname === '/posts/new') {
      this.setState({ activeItem: 'add post' })
    }
  }

  render() {
    const { activeItem } = this.state

    return (
      <Menu pointing secondary>
        <Menu.Item
          name='home'
          active={activeItem === 'home'}
          onClick={this.handleItemClick}
          routeto='/posts'
        />
        <Menu.Item
          name='add post'
          active={activeItem === 'add post'}
          onClick={this.handleItemClick}
          routeto='/posts/new'
        />
      </Menu>
    );
  }
}

export default withRouter(Header);
