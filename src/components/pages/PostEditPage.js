import React from 'react';
import PostForm from '../post/PostForm';
import { connect } from 'react-redux';
import { fetchPost, changePost } from '../../actions/post';

class PostEditPage extends React.Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  render() {
    return (
      <PostForm post={this.props.post} newPost={this.props.changePost} />
    );
  }
}

export default connect((state) => ({post: state.post.post}), { fetchPost, changePost })(PostEditPage);
