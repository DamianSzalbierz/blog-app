import React from 'react';
import PostForm from '../post/PostForm';
import { connect } from 'react-redux';
import { newPost } from '../../actions/post';

class PostNewPage extends React.Component {

  render() {
    return (
      <PostForm newPost={this.props.newPost} />
    );
  }
}

export default connect(null, { newPost })(PostNewPage);
