import React from 'react';
import { Segment } from 'semantic-ui-react';
import { connect } from 'react-redux';
import { fetchPost } from '../../actions/post';

class PostPage extends React.Component {
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.fetchPost(id);
  }

  render() {
    return (
      <Segment.Group>
        <Segment>{this.props.post.author}</Segment>
        <Segment>{this.props.post.title}</Segment>
        <Segment>{this.props.post.content}</Segment>
      </Segment.Group>
    );
  }
}

export default connect((state) => ({post: state.post.post}), { fetchPost })(PostPage);
