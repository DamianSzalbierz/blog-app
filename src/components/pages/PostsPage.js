import React from 'react';
import PostList from '../post/PostList';

class PostsPage extends React.Component {

  render() {
    return (
      <div>
        <h1>Welcome to Posts Page!</h1>
        <PostList />
      </div>
    );
  }
}

export default PostsPage;
