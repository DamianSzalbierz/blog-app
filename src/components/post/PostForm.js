import React from "react";
import PropTypes from 'prop-types';
import { Form } from "semantic-ui-react";
import './post.css';
import InlineError from '../messages/InlineError';
import { withRouter } from 'react-router-dom';

class PostForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: {
        author: "",
        title: "",
        content: ""
      },
      errors: {},
      loading: false
    };
  }

  componentDidMount() {
    if (this.props.post) this.setState({
      data: {
        id: this.props.post.id || '',
        author: this.props.post.author || '',
        title: this.props.post.title || '',
        content: this.props.post.content || '',
      }
    })
  }

  handleChange = (e) => {
    const { name, value } = e.target;
    this.setState({ data: { ...this.state.data, [name]: value } });
  }

  handleSubmit = e => {
    console.log(this.state.data)
    const errors = this.validate(this.state.data);
    this.setState({errors})
    if (Object.keys(errors).length === 0) {
      this.props.newPost(this.state.data, () => {
        this.props.history.push(`/`)
      })
    }
  }

  validate = (data) => {
    const errors = {};
    if (!data.author) errors.author = 'Should not be empty'
    if (!data.title) errors.title = 'Should not be empty'
    if (!data.content) errors.content = 'Should not be empty'
    return errors;
  }

  render() {
    const { data, loading, errors } = this.state;

    return (
      <div>
        <h2>{this.props.post ? 'Edit ' : 'New '}Post</h2>
        <Form onSubmit={this.handleSubmit} loading={loading}>
          <Form.Field error={!!errors.author} width="4">
            <label>Author</label>
            <input
              value={data.author}
              onChange={this.handleChange}
              name="author"
              placeholder="Author"
            />
            {errors.author && <InlineError text={errors.author} />}
          </Form.Field>
          <Form.Field error={!!errors.title}>
            <label>Title</label>
            <input
              onChange={this.handleChange}
              value={data.title}
              name="title"
              placeholder="Title"
            />
            {errors.title && <InlineError text={errors.title} />}
          </Form.Field>
          <Form.TextArea
            className='postForm'
            error={!!errors.content}
            onChange={this.handleChange}
            value={data.content}
            name="content"
            label="Content"
            placeholder="Content"
          />
          {errors.content && <InlineError text={errors.content} />}
          <Form.Button>{this.props.post ? 'Update ' : 'Add '}</Form.Button>
        </Form>

      </div>
    );
  }
}

PostForm.propTypes = {
  newPost: PropTypes.func.isRequired,
};

export default withRouter(PostForm);
