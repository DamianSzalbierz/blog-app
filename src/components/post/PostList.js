import React from "react";
import { connect } from "react-redux";
import { fetchPosts, deletePost } from "../../actions/post";
import { Button } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const PostItem = ({id, author, title, deletePost}) => {
  return (
    <li key={id}>
      <p>{author}</p>
      <p>{title}</p>
      <Link to={`/posts/${id}`}><Button color='green' content='show' /></Link>
      <Link to={`/posts/${id}/edit`}><Button color='yellow' content='edit' /></Link>
      <Button onClick={() => deletePost(id)} color='red' content='delete' />
      <hr />
    </li>
  );
}

class PostList extends React.Component {
  componentDidMount = () => {
    this.props.fetchPosts();
  };

  render() {
    const { posts } = this.props;

    return (
      <ul>
        {posts.map(post => <PostItem deletePost={this.props.deletePost} key={post.id} {...post} />)}
      </ul>
    );
  }
}

export default connect((state) => ({ posts: state.post.posts }), { fetchPosts, deletePost })(
  PostList
);
