export const getPosts = () => {
  return fetch('http://localhost:8080/posts')
    .then(res => res.json())
}

export const getPost = id => {
  return fetch(`http://localhost:8080/posts/${id}`)
    .then(res => res.json())
}

export const removePost = id => {
  return fetch(`http://localhost:8080/posts/${id}`, {
    method: 'delete',
    headers: {
      "Accept": 'application/json',
      "Content-Type": 'application/json'
    }
  })
}

export const updatePost = post => {
  return fetch(`http://localhost:8080/posts/${post.id}`, {
    method: 'put',
    headers: {
      "Accept": 'application/json',
      "Content-Type": 'application/json'
    },
    body: JSON.stringify({...post})
  })
    .then(res => res.json())
}

export const savePost = (post) => {
  return fetch('http://localhost:8080/posts', {
    method: 'post',
    headers: {
      "Accept": "application/json",
      "Content-Type": 'application/json'
    },
    body: JSON.stringify({...post})
  })
    .then(res => res.json())
}
