export const POSTS_LOAD = "FETCH_POSTS";
export const POST_ADD = "POST_ADD";
export const POST_SHOW = "POST_SHOW";
export const POST_UPDATE = "POST_UPDATE";
export const POST_DELETE = "POST_DELETE";
