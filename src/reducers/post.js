import {
  POSTS_LOAD,
  POST_ADD,
  POST_SHOW,
  POST_UPDATE,
  POST_DELETE
} from "../lib/types";

const initialState = {
  posts: [],
  post: {}
};

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case POSTS_LOAD:
      return { ...state, posts: action.payload };
    case POST_ADD:
      return { ...state, posts: [...state.posts, action.payload] };
    case POST_SHOW:
      return { ...state, post: action.payload };
    case POST_UPDATE:
      return {
        ...state,
        posts: [
          ...state.posts.filter(
            post => (post.id !== action.payload.id ? post : action.payload)
          )
        ]
      };
    case POST_DELETE:
      return {
        ...state,
        posts: [...state.posts.filter(post => post.id !== action.payload)]
      };
    default:
      return state;
  }
};
