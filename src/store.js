import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import postReducer from './reducers/post';
import { composeWithDevTools } from 'redux-devtools-extension';

const reducer = combineReducers({
  post: postReducer
})

export default createStore(
  reducer,
  composeWithDevTools(
    applyMiddleware(thunk)
  )
);
